package ai.commerce.xpresso.v1.utils;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Created by naveen on 11/5/17.
 */
public class ErrorHandler {

    public static JSONObject generateError(int code, String status, String msg){
        JSONObject finalResults = new JSONObject();
        try {
            finalResults.put("code",code);
            finalResults.put("status",status);
            finalResults.put("msg",msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return finalResults;
    }
}
