
package ai.commerce.xpresso.v1_1.amazon.dynamodb;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;

public class XCFashionProductCatalog extends Item {
	static Item item;

	public static void setProduct(String sku, String brand, String productName, String price, String details, String features, String imgSrc, String gender, String category, String subCategory, String section, String color) {
		item = new Item()
				.withPrimaryKey("sku", sku)
				.withString("brand", brand)
				.withString("productname", productName)
				.withString("price", price)
				.withString("details", details)
				.withString("features", features)
				.withString("image", "https://s3.amazonaws.com/debenhams17ba2c94-4bff-4c8a-b61c-99682a9e3553/" + sku)
				.withString("gender", gender)
				.withString("category", category)
				.withString("subcategory", subCategory)
				.withString("section", section)
				.withStringSet("colorsavailable", new HashSet<String>(Arrays.asList(color.split(","))));

		System.out.println(item.toJSONPretty());
	}

	public static void setProduct(String sku, String brand, String productName, String price, String details, String features, String imgSrc, String gender, String category, String subCategory, String color) {
		item = new Item()
				.withPrimaryKey("sku", sku)
				.withString("brand", brand)
				.withString("productname", productName)
				.withString("price", price)
				.withString("details", details)
				.withString("features", features)
				.withString("image", "https://s3.amazonaws.com/debenhams17ba2c94-4bff-4c8a-b61c-99682a9e3553/" + sku)
				.withString("gender", gender)
				.withString("category", category)
				.withString("subcategory", subCategory)
				.withStringSet("colorsavailable", new HashSet<String>(Arrays.asList(color.split(","))));
		System.out.println(item.toJSONPretty());
	}

	public static void setProduct(String sku, String brand, String productName, String price, String details, String features, String imgSrc, String gender, String category, String color) {
		item = new Item()
				.withPrimaryKey("sku", sku)
				.withString("brand", brand)
				.withString("productname", productName)
				.withString("price", price)
				.withString("details", details)
				.withString("features", features)
				.withString("image", "https://s3.amazonaws.com/debenhams17ba2c94-4bff-4c8a-b61c-99682a9e3553/" + sku)
				.withString("gender", gender)
				.withString("category", category)
				.withStringSet("colorsavailable", new HashSet<String>(Arrays.asList(color.split(","))));
		System.out.println(item.toJSONPretty());
	}

	public static Item getProduct() {
		return item;
	}

	public static String getProductStr() {
		return item.toJSON();
	}

	public static Map<String, Object> getProductAsMap() {
		return item.asMap();
	}
}
