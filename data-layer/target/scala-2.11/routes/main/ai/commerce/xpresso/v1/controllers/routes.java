
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

package ai.commerce.xpresso.v1.controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final ai.commerce.xpresso.v1.controllers.ReverseDynamoDBGet DynamoDBGet = new ai.commerce.xpresso.v1.controllers.ReverseDynamoDBGet(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCloudSearchGet CloudSearchGet = new ai.commerce.xpresso.v1.controllers.ReverseCloudSearchGet(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCatalogPut CatalogPut = new ai.commerce.xpresso.v1.controllers.ReverseCatalogPut(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseDynamoDBPost DynamoDBPost = new ai.commerce.xpresso.v1.controllers.ReverseDynamoDBPost(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCatalogDelete CatalogDelete = new ai.commerce.xpresso.v1.controllers.ReverseCatalogDelete(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseUtilityGet UtilityGet = new ai.commerce.xpresso.v1.controllers.ReverseUtilityGet(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReversePromotionsGet PromotionsGet = new ai.commerce.xpresso.v1.controllers.ReversePromotionsGet(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCloudSearchPut CloudSearchPut = new ai.commerce.xpresso.v1.controllers.ReverseCloudSearchPut(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseBrandsOnRedis BrandsOnRedis = new ai.commerce.xpresso.v1.controllers.ReverseBrandsOnRedis(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseDynamoDBPut DynamoDBPut = new ai.commerce.xpresso.v1.controllers.ReverseDynamoDBPut(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCatalogPost CatalogPost = new ai.commerce.xpresso.v1.controllers.ReverseCatalogPost(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCloudSearchDelete CloudSearchDelete = new ai.commerce.xpresso.v1.controllers.ReverseCloudSearchDelete(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseElasticSearchGet ElasticSearchGet = new ai.commerce.xpresso.v1.controllers.ReverseElasticSearchGet(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseDynamoDBDelete DynamoDBDelete = new ai.commerce.xpresso.v1.controllers.ReverseDynamoDBDelete(RoutesPrefix.byNamePrefix());
  public static final ai.commerce.xpresso.v1.controllers.ReverseCollectionsGet CollectionsGet = new ai.commerce.xpresso.v1.controllers.ReverseCollectionsGet(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBGet DynamoDBGet = new ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBGet(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCloudSearchGet CloudSearchGet = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCloudSearchGet(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCatalogPut CatalogPut = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCatalogPut(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBPost DynamoDBPost = new ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBPost(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCatalogDelete CatalogDelete = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCatalogDelete(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseUtilityGet UtilityGet = new ai.commerce.xpresso.v1.controllers.javascript.ReverseUtilityGet(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReversePromotionsGet PromotionsGet = new ai.commerce.xpresso.v1.controllers.javascript.ReversePromotionsGet(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCloudSearchPut CloudSearchPut = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCloudSearchPut(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseBrandsOnRedis BrandsOnRedis = new ai.commerce.xpresso.v1.controllers.javascript.ReverseBrandsOnRedis(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBPut DynamoDBPut = new ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBPut(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCatalogPost CatalogPost = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCatalogPost(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCloudSearchDelete CloudSearchDelete = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCloudSearchDelete(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseElasticSearchGet ElasticSearchGet = new ai.commerce.xpresso.v1.controllers.javascript.ReverseElasticSearchGet(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBDelete DynamoDBDelete = new ai.commerce.xpresso.v1.controllers.javascript.ReverseDynamoDBDelete(RoutesPrefix.byNamePrefix());
    public static final ai.commerce.xpresso.v1.controllers.javascript.ReverseCollectionsGet CollectionsGet = new ai.commerce.xpresso.v1.controllers.javascript.ReverseCollectionsGet(RoutesPrefix.byNamePrefix());
  }

}
