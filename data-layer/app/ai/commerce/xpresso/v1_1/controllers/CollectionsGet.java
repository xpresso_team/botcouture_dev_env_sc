package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB;
import ai.commerce.xpresso.v1_1.data.DynamoDBData;
import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all GET request for the AWS Dynamodb.
 * Uses AWS SDK to create a filtered or normal query to fetch products
 *
 * It has following api:
 *  1) scan All products
 *  2) get single products
 */

public class CollectionsGet extends Controller {

    @Inject
    private Configuration mConfiguration;

    private ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB mAmazonDynamoDB = null;

    @Inject
    CollectionsGet(Configuration configuration){
        mAmazonDynamoDB = new ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB(configuration);
    }

    public Result l1() {

        final Set<Map.Entry<String,String[]>> entries = request().queryString().entrySet();

        String gender = "women";
        String type = "collections";
        for (Map.Entry<String,String[]> entry : entries) {
            if ( entry.getKey().equalsIgnoreCase("gender") &&
                    entry.getValue().length > 0){
                
            } else if ( entry.getKey().equalsIgnoreCase("type") &&
                    entry.getValue().length > 0){

            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }
        return null;
    }

    public Result l2() {
        return null;
    }

    public Result l3() {
        return null;
    }


    }