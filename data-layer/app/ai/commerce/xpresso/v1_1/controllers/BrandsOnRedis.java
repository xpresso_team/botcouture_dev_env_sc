package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearch;
import ai.commerce.xpresso.v1_1.search.SearchItem;
import ai.commerce.xpresso.v1_1.search.XCCloudSearchResult;
import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Result;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.io.IOException;
import java.util.*;

import static play.mvc.Controller.request;
import static play.mvc.Results.ok;

/**
 * Created by mayank on 16/5/17.
 */
public class BrandsOnRedis {
    @Inject
    private Configuration mConfiguration;

    private ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearch amazonCloudSearch;
    private static JedisPool pool;
    private String redisPassword;

    @Inject
    public BrandsOnRedis(Configuration configuration){
        amazonCloudSearch = new ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearch(configuration);
    }

    private void initialize()
    {
        try {
            if (pool == null)
                pool = new JedisPool(new JedisPoolConfig(), mConfiguration.getString("datalayer.redis.redis_server_ip")); // host ip where redis is installed
        }
        catch (Exception e)
        {
            Logger.error("Error in connecting to redis : "+e.getMessage());
        }
    }

    public Result updateDeltaUpdateForBrand()
    {
        initialize();
        JSONObject brandJson = null;
        try {
            brandJson = new JSONObject(request().body().asJson().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Check if productData is null
        if(brandJson == null){
            return ok(ai.commerce.xpresso.v1_1.utils.ErrorHandler
                    .generateError(400, "fail", "Invalid Json")
                    .toString()).as("application/json");
        }
        JSONArray brandList = null;
        try {
            if (brandJson.has("brand") && brandJson.getJSONArray("brand").length() > 0) {
                try {
                    brandList = brandJson.getJSONArray("brand");
                } catch (JSONException e) {
                    Logger.error(e.getMessage());
                }
            }
        }
        catch (JSONException e)
        {
            return ok(ai.commerce.xpresso.v1_1.utils.ErrorHandler
                    .generateError(400, "fail", "Invalid Json")
                    .toString()).as("application/json");
        }
        Set<String> brandFromDB = new HashSet<String>();
        if (brandList != null) {
            for (int i=0;i<brandList.length();i++){
                try {
                    brandFromDB.add(brandList.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        Set<String> brandFromRedis = getBrandFromRedis();
        brandFromDB.removeAll(brandFromRedis);
        Logger.info("Number of brands to add in redis : "+brandFromDB.size());
        addToRedis(brandFromDB);
        return ok();
    }

    public Result updateBrand()
    {
        initialize();
        Set<String> brandFromRedis = getBrandFromRedis();
        String cloudSearchDomain = mConfiguration.getString("datalayer.cloudsearch.default_domain");
        String[] returnField = {
                mConfiguration.getString("datalayer.redis.return_field")
        };

        Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();
        Set<String> filterStrings = new HashSet<>();
        filterStrings.add("men");
        filterStrings.add("women");
        filterMap.put("gender",filterStrings);

        ai.commerce.xpresso.v1_1.search.XCCloudSearchResult xcCloudSearchResult = amazonCloudSearch.getSearchItems(returnField,
                cloudSearchDomain,
                filterMap, 10000, "initial",false);
        while(xcCloudSearchResult.mResultList.size() > 0)
        {
            List<ai.commerce.xpresso.v1_1.search.SearchItem> searchItems = xcCloudSearchResult.getResults();
            HashSet<String> brandsList = new HashSet();
            String brand = null;
            Logger.info("Size of search items : "+searchItems.size());
            for(ai.commerce.xpresso.v1_1.search.SearchItem searchItem:searchItems)
            {
                brand = searchItem.getAttributeValue("brand");
                if(brand != null && !brand.isEmpty())
                    brandsList.add(brand);
            }
            Logger.info("Get unique brands "+brandsList.size() + " from a batch of "+searchItems.size() + " from elastic search");
            if(brandsList .size() > 0) {
                brandsList.removeAll(brandFromRedis);
                Logger.info("Batch of brands to add in redis : " + brandsList.size());
                // pipeline use to do batch wise addition to the redis on sync
                addToRedis(brandsList);
            }
            xcCloudSearchResult = amazonCloudSearch.getSearchItems(returnField,
                    cloudSearchDomain, filterMap, 10000, xcCloudSearchResult.mCursor, false);
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Redis Brand Size before update", brandFromRedis.size());
            jsonObject.put("Redis Brand Size after update", getBrandFromRedis().size());
        } catch (JSONException e) {
            Logger.error(" Error in fetching info from redis : " + e.getMessage());
        }
        return ok(jsonObject.toString()).as("application/json");
    }


    private void addToRedis(Set<String> brandList) {
        // pipeline use to do batch wise addition to the redis on sync
        Jedis jedis = getJedis();
        Pipeline pipelined = jedis.pipelined();
        if (brandList != null) {
            for (String brand : brandList) {
                pipelined.sadd("brand", brand);
            }
        }
        pipelined.sync();
        try {
            pipelined.close();
        } catch (IOException e) {
            Logger.error(" Error in adding to redis "+ e.getMessage());
        }
        finally {
            closeJedis(jedis);
        }
    }

    private List<String> getBrandFromDB() {
        String cloudSearchDomain = mConfiguration.getString("datalayer.cloudsearch.default_domain");
        String[] returnField = {
                mConfiguration.getString("datalayer.redis.return_field")
        };

        Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();
        Set<String> filterStrings = new HashSet<>();
        filterStrings.add("men");
        filterStrings.add("women");
        filterMap.put("gender",filterStrings);

        ai.commerce.xpresso.v1_1.search.XCCloudSearchResult xcCloudSearchResult = amazonCloudSearch.getSearchItems(returnField,
                                                                        cloudSearchDomain,
                                                                        filterMap, 10000, "initial",false);

        List<ai.commerce.xpresso.v1_1.search.SearchItem> searchItems = xcCloudSearchResult.getResults();
        List<String> brandsList = new ArrayList<>();
        String brand = null;
        Logger.info("Size of search items : "+searchItems.size());
        for(ai.commerce.xpresso.v1_1.search.SearchItem searchItem:searchItems)
        {
            brand = searchItem.getAttributeValue("brand");
            if(brand != null && !brand.isEmpty())
                brandsList.add(brand);
        }
        Logger.info("Get unique brands "+brandsList.size() + " from a batch of "+searchItems.size() + " from elastic search");
        return brandsList;
    }

    public Result getRedisBrandList()
    {
        initialize();
        Set<String> brand = getBrandFromRedis();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("brand",brand);
            jsonObject.put("size",brand.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ok(jsonObject.toString()).as("application/json");
    }

    private Set<String> getBrandFromRedis() {
        Set<String> brand = new HashSet<>();
        Jedis jedis = getJedis();
        if(jedis.exists("brand"))
        {
            brand = jedis.smembers("brand");
        }
        Logger.info(" Brand list size in redis : "+brand.size());
        closeJedis(jedis);
        return brand;
    }

    private Jedis getJedis() {
        Jedis j = pool.getResource();
        redisPassword = mConfiguration.getString("datalayer.redis.redis_server_password");
        if (redisPassword != null) {
            j.auth(redisPassword);
        }
        return j;
    }

    private void closeJedis(Jedis j) {
        j.close();
    }
}
