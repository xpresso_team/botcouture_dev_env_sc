package ai.commerce.xpresso.v1.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ai.commerce.xpresso.v1.search.SearchItem;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

/**
 * An instance of this class represents a query.
 * This instance holds the original query as well as its spell checked version,
 * the entities extracted from this query and the retrieved corresponding results.
 * @author Alix Melchy Aug 31, 2016
 *
 */
public class XCQuery {
	private String globalPriceStr;
	private String domainName;
	private String queryText;
	private String originalQuery;
	private String spellCorrectedQuery;
	private String clientName;
	private int pageNo;
	private int perPage;
	private List<SearchItem> simpleSearchResults;
	private List<SearchItem> structuredSearchResults;
	private Map<String, XCEntity> entityMap = null;
	private boolean hasAllCategories = false;
	private Set<String> uniqueEntitySet = null;
	private static Logger logger;
	private JSONObject intent;
	private JSONObject clarification;
	public boolean isChatBotQuery;
	String customReturnAttributes;
	private HashMap<String, Double> sizeTokensWithConfidence;

	public static void init() {
		logger = XCLogger.getSpLogger();
	}

	public XCQuery(String originalQuery, String spellCorrectedQuery, String queryText, String clientName, String domainName, boolean isChatbotQuery, String customReturnAttributes, int pageNo, int perPage) {
		//		super(queryText);
		this.spellCorrectedQuery = spellCorrectedQuery;
		this.queryText = queryText;
		this.pageNo = pageNo;
		this.perPage = perPage;
		this.domainName = domainName;
		this.clientName = clientName;
		this.originalQuery = originalQuery;
		this.entityMap = new HashMap<String, XCEntity>();
		this.uniqueEntitySet = new HashSet<String>();
		this.intent = new JSONObject();
		this.clarification = new JSONObject();
		this.isChatBotQuery = isChatbotQuery;
		this.customReturnAttributes = customReturnAttributes;

		/*		this.parseQuery();
		*/
	}

	/**
	 * @return the normalized string of this query
	 */
	//	public String getQueryString() {
	//		return this.normalized_text;
	//	}

	public String getOriginalQuery() {
		return this.originalQuery;
	}

	/**
	 * @return the number of result pages to display
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * Sets the number of pages of results to display
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @return the number of items per result page
	 */
	public int getPerPage() {
		return perPage;
	}

	/**
	 * Sets the number of items per result page
	 * @param perPage
	 */
	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public String getClientName() {
		if (this.clientName.equals("") || this.clientName.equalsIgnoreCase("all") || this.clientName == null || this.clientName.isEmpty())
			this.clientName = "";
		return this.clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the domain (customer) name
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * Sets the domain (customer) name for this query
	 * @param domainName
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * This query text is the lemmatized version of the original query.
	 * @return this query text
	 */
	public String getQueryText() {
		return queryText;
	}

	/**
	 * Sets this query text, which is a processed version of the normalized query string.
	 * So far, holds the lemmatized version of the original query.
	 * @param queryText
	 */
	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	/**
	 * @return spell corrected version of this query text
	 */
	public String getSpellCorrectedQuery() {
		return spellCorrectedQuery;
	}

	/**
	 * Sets this spell corrected version of this query text
	 * @param spellCorrectedQuery
	 */
	public void setSpellCorrectedQuery(String spellCorrectedQuery) {
		this.spellCorrectedQuery = spellCorrectedQuery;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param intent
	 */
	public void setIntent(JSONObject intent) {
		this.intent = intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getIntent() {
		return intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param clarification
	 */
	public void setClarification(JSONObject clarification) {
		this.clarification = clarification;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getClarification() {
		return clarification;
	}

	/**
	 * This entity map associates the identified entity string with its {@link com.abzooba.xcommerce.core.XCEntity#XCEntity(String, XCQuery, boolean)}
	 * @return entity map
	 */
	public Map<String, XCEntity> getEntityMap() {
		return entityMap;
	}

	/**
	 * Adds this spEntity with key entity to this entity map.
	 * This entity map associates the identified entity string with its {@link com.abzooba.xcommerce.core.XCEntity#XCEntity(String, XCQuery, boolean)}
	 * @param entity
	 * @param spEntity
	 */
	public void addToEntityMap(String entity, XCEntity spEntity) {
		this.entityMap.put(entity, spEntity);
	}

}
