
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

package ai.commerce.xpresso.v1.swagger;

import router.RoutesPrefix;

public class routes {
  
  public static final ai.commerce.xpresso.v1.swagger.ReverseSwaggerController SwaggerController = new ai.commerce.xpresso.v1.swagger.ReverseSwaggerController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final ai.commerce.xpresso.v1.swagger.javascript.ReverseSwaggerController SwaggerController = new ai.commerce.xpresso.v1.swagger.javascript.ReverseSwaggerController(RoutesPrefix.byNamePrefix());
  }

}
