
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  Application_16: controllers.Application,
  // @LINE:9
  ElasticSearchGet_1: ai.commerce.xpresso.v1.controllers.ElasticSearchGet,
  // @LINE:15
  CloudSearchGet_12: ai.commerce.xpresso.v1.controllers.CloudSearchGet,
  // @LINE:16
  CloudSearchGet_2: ai.commerce.xpresso.v1_1.controllers.CloudSearchGet,
  // @LINE:20
  DynamoDBGet_17: ai.commerce.xpresso.v1.controllers.DynamoDBGet,
  // @LINE:21
  CatalogPut_10: ai.commerce.xpresso.v1.controllers.CatalogPut,
  // @LINE:22
  CatalogPost_3: ai.commerce.xpresso.v1.controllers.CatalogPost,
  // @LINE:23
  CatalogDelete_6: ai.commerce.xpresso.v1.controllers.CatalogDelete,
  // @LINE:29
  DynamoDBPut_14: ai.commerce.xpresso.v1.controllers.DynamoDBPut,
  // @LINE:30
  DynamoDBPost_7: ai.commerce.xpresso.v1.controllers.DynamoDBPost,
  // @LINE:31
  DynamoDBDelete_0: ai.commerce.xpresso.v1.controllers.DynamoDBDelete,
  // @LINE:38
  CloudSearchDelete_4: ai.commerce.xpresso.v1.controllers.CloudSearchDelete,
  // @LINE:39
  CloudSearchPut_9: ai.commerce.xpresso.v1.controllers.CloudSearchPut,
  // @LINE:42
  UtilityGet_13: ai.commerce.xpresso.v1.controllers.UtilityGet,
  // @LINE:45
  BrandsOnRedis_5: ai.commerce.xpresso.v1.controllers.BrandsOnRedis,
  // @LINE:50
  CollectionsGet_11: ai.commerce.xpresso.v1.controllers.CollectionsGet,
  // @LINE:55
  PromotionsGet_18: ai.commerce.xpresso.v1.controllers.PromotionsGet,
  // @LINE:58
  SwaggerController_8: ai.commerce.xpresso.v1.swagger.SwaggerController,
  // @LINE:59
  Assets_15: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    Application_16: controllers.Application,
    // @LINE:9
    ElasticSearchGet_1: ai.commerce.xpresso.v1.controllers.ElasticSearchGet,
    // @LINE:15
    CloudSearchGet_12: ai.commerce.xpresso.v1.controllers.CloudSearchGet,
    // @LINE:16
    CloudSearchGet_2: ai.commerce.xpresso.v1_1.controllers.CloudSearchGet,
    // @LINE:20
    DynamoDBGet_17: ai.commerce.xpresso.v1.controllers.DynamoDBGet,
    // @LINE:21
    CatalogPut_10: ai.commerce.xpresso.v1.controllers.CatalogPut,
    // @LINE:22
    CatalogPost_3: ai.commerce.xpresso.v1.controllers.CatalogPost,
    // @LINE:23
    CatalogDelete_6: ai.commerce.xpresso.v1.controllers.CatalogDelete,
    // @LINE:29
    DynamoDBPut_14: ai.commerce.xpresso.v1.controllers.DynamoDBPut,
    // @LINE:30
    DynamoDBPost_7: ai.commerce.xpresso.v1.controllers.DynamoDBPost,
    // @LINE:31
    DynamoDBDelete_0: ai.commerce.xpresso.v1.controllers.DynamoDBDelete,
    // @LINE:38
    CloudSearchDelete_4: ai.commerce.xpresso.v1.controllers.CloudSearchDelete,
    // @LINE:39
    CloudSearchPut_9: ai.commerce.xpresso.v1.controllers.CloudSearchPut,
    // @LINE:42
    UtilityGet_13: ai.commerce.xpresso.v1.controllers.UtilityGet,
    // @LINE:45
    BrandsOnRedis_5: ai.commerce.xpresso.v1.controllers.BrandsOnRedis,
    // @LINE:50
    CollectionsGet_11: ai.commerce.xpresso.v1.controllers.CollectionsGet,
    // @LINE:55
    PromotionsGet_18: ai.commerce.xpresso.v1.controllers.PromotionsGet,
    // @LINE:58
    SwaggerController_8: ai.commerce.xpresso.v1.swagger.SwaggerController,
    // @LINE:59
    Assets_15: controllers.Assets
  ) = this(errorHandler, Application_16, ElasticSearchGet_1, CloudSearchGet_12, CloudSearchGet_2, DynamoDBGet_17, CatalogPut_10, CatalogPost_3, CatalogDelete_6, DynamoDBPut_14, DynamoDBPost_7, DynamoDBDelete_0, CloudSearchDelete_4, CloudSearchPut_9, UtilityGet_13, BrandsOnRedis_5, CollectionsGet_11, PromotionsGet_18, SwaggerController_8, Assets_15, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Application_16, ElasticSearchGet_1, CloudSearchGet_12, CloudSearchGet_2, DynamoDBGet_17, CatalogPut_10, CatalogPost_3, CatalogDelete_6, DynamoDBPut_14, DynamoDBPost_7, DynamoDBDelete_0, CloudSearchDelete_4, CloudSearchPut_9, UtilityGet_13, BrandsOnRedis_5, CollectionsGet_11, PromotionsGet_18, SwaggerController_8, Assets_15, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.Application.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/elasticsearch/genericAPI/""", """ai.commerce.xpresso.v1.controllers.ElasticSearchGet.genericAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/elasticsearch/structuredSearch/""", """ai.commerce.xpresso.v1.controllers.ElasticSearchGet.structuredSearchAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/catalog/genericAPI/""", """ai.commerce.xpresso.v1.controllers.CloudSearchGet.genericAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1.1/catalog/genericAPI/""", """ai.commerce.xpresso.v1_1.controllers.CloudSearchGet.genericAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/catalog/structuredSearch/""", """ai.commerce.xpresso.v1.controllers.CloudSearchGet.structuredSearchAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/catalog/product/""", """ai.commerce.xpresso.v1.controllers.DynamoDBGet.getProduct"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/catalog/product/""", """ai.commerce.xpresso.v1.controllers.CatalogPut.insertBatch"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/catalog/product/""", """ai.commerce.xpresso.v1.controllers.CatalogPost.updateProduct"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/catalog/product/""", """ai.commerce.xpresso.v1.controllers.CatalogDelete.deleteProduct"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/dynamodb/scan/""", """ai.commerce.xpresso.v1.controllers.DynamoDBGet.scanAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/dynamodb/product/""", """ai.commerce.xpresso.v1.controllers.DynamoDBGet.getProduct"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/dynamodb/product/""", """ai.commerce.xpresso.v1.controllers.DynamoDBPut.insertBatch"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/dynamodb/product/""", """ai.commerce.xpresso.v1.controllers.DynamoDBPost.updateProduct"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/dynamodb/product/""", """ai.commerce.xpresso.v1.controllers.DynamoDBDelete.deleteProduct"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/cloudsearch/genericAPI/""", """ai.commerce.xpresso.v1.controllers.CloudSearchGet.genericAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1.1/cloudsearch/genericAPI/""", """ai.commerce.xpresso.v1_1.controllers.CloudSearchGet.genericAPI"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/cloudsearch/structuredSearch/""", """ai.commerce.xpresso.v1.controllers.CloudSearchGet.structuredSearchAPI"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/cloudsearch/genericAPI/""", """ai.commerce.xpresso.v1.controllers.CloudSearchDelete.deleteProduct"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/cloudsearch/put/""", """ai.commerce.xpresso.v1.controllers.CloudSearchPut.insertItem"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/util/brand/""", """ai.commerce.xpresso.v1.controllers.UtilityGet.getBrand"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/redis/updateBrands""", """ai.commerce.xpresso.v1.controllers.BrandsOnRedis.updateBrand"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/redis/getBrands""", """ai.commerce.xpresso.v1.controllers.BrandsOnRedis.getRedisBrandList"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/redis/updateDeltaBrands""", """ai.commerce.xpresso.v1.controllers.BrandsOnRedis.updateDeltaUpdateForBrand"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/collections/l1/""", """ai.commerce.xpresso.v1.controllers.CollectionsGet.l1"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/collections/l2/""", """ai.commerce.xpresso.v1.controllers.CollectionsGet.l2"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/collections/l3/""", """ai.commerce.xpresso.v1.controllers.CollectionsGet.l3"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """v1/promotions/todayuser/""", """ai.commerce.xpresso.v1.controllers.PromotionsGet.getTodaysPromotions"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """docs/swagger.json""", """ai.commerce.xpresso.v1.swagger.SwaggerController.getResource"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """docs/""", """controllers.Assets.at(path:String = "/public/swagger-ui", file:String = "index.html")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """docs/""" + "$" + """file<.+>""", """controllers.Assets.at(path:String = "/public/swagger-ui", file:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_Application_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Application_index0_invoker = createInvoker(
    Application_16.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "index",
      Nil,
      "GET",
      """ Home page""",
      this.prefix + """"""
    )
  )

  // @LINE:9
  private[this] lazy val ai_commerce_xpresso_v1_controllers_ElasticSearchGet_genericAPI1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/elasticsearch/genericAPI/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_ElasticSearchGet_genericAPI1_invoker = createInvoker(
    ElasticSearchGet_1.genericAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.ElasticSearchGet",
      "genericAPI",
      Nil,
      "GET",
      """ ElasticSearchClient based routes""",
      this.prefix + """v1/elasticsearch/genericAPI/"""
    )
  )

  // @LINE:10
  private[this] lazy val ai_commerce_xpresso_v1_controllers_ElasticSearchGet_structuredSearchAPI2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/elasticsearch/structuredSearch/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_ElasticSearchGet_structuredSearchAPI2_invoker = createInvoker(
    ElasticSearchGet_1.structuredSearchAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.ElasticSearchGet",
      "structuredSearchAPI",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/elasticsearch/structuredSearch/"""
    )
  )

  // @LINE:15
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/catalog/genericAPI/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI3_invoker = createInvoker(
    CloudSearchGet_12.genericAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CloudSearchGet",
      "genericAPI",
      Nil,
      "GET",
      """ Product
 Search engine based routes""",
      this.prefix + """v1/catalog/genericAPI/"""
    )
  )

  // @LINE:16
  private[this] lazy val ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1.1/catalog/genericAPI/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI4_invoker = createInvoker(
    CloudSearchGet_2.genericAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1_1.controllers.CloudSearchGet",
      "genericAPI",
      Nil,
      "GET",
      """""",
      this.prefix + """v1.1/catalog/genericAPI/"""
    )
  )

  // @LINE:18
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/catalog/structuredSearch/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI5_invoker = createInvoker(
    CloudSearchGet_12.structuredSearchAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CloudSearchGet",
      "structuredSearchAPI",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/catalog/structuredSearch/"""
    )
  )

  // @LINE:20
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/catalog/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct6_invoker = createInvoker(
    DynamoDBGet_17.getProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.DynamoDBGet",
      "getProduct",
      Nil,
      "GET",
      """ Catalog search based routes""",
      this.prefix + """v1/catalog/product/"""
    )
  )

  // @LINE:21
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CatalogPut_insertBatch7_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/catalog/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CatalogPut_insertBatch7_invoker = createInvoker(
    CatalogPut_10.insertBatch,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CatalogPut",
      "insertBatch",
      Nil,
      "PUT",
      """""",
      this.prefix + """v1/catalog/product/"""
    )
  )

  // @LINE:22
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CatalogPost_updateProduct8_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/catalog/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CatalogPost_updateProduct8_invoker = createInvoker(
    CatalogPost_3.updateProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CatalogPost",
      "updateProduct",
      Nil,
      "POST",
      """""",
      this.prefix + """v1/catalog/product/"""
    )
  )

  // @LINE:23
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CatalogDelete_deleteProduct9_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/catalog/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CatalogDelete_deleteProduct9_invoker = createInvoker(
    CatalogDelete_6.deleteProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CatalogDelete",
      "deleteProduct",
      Nil,
      "DELETE",
      """""",
      this.prefix + """v1/catalog/product/"""
    )
  )

  // @LINE:26
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBGet_scanAll10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/dynamodb/scan/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBGet_scanAll10_invoker = createInvoker(
    DynamoDBGet_17.scanAll,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.DynamoDBGet",
      "scanAll",
      Nil,
      "GET",
      """ Dynamodb based routes""",
      this.prefix + """v1/dynamodb/scan/"""
    )
  )

  // @LINE:28
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/dynamodb/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct11_invoker = createInvoker(
    DynamoDBGet_17.getProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.DynamoDBGet",
      "getProduct",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/dynamodb/product/"""
    )
  )

  // @LINE:29
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBPut_insertBatch12_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/dynamodb/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBPut_insertBatch12_invoker = createInvoker(
    DynamoDBPut_14.insertBatch,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.DynamoDBPut",
      "insertBatch",
      Nil,
      "PUT",
      """""",
      this.prefix + """v1/dynamodb/product/"""
    )
  )

  // @LINE:30
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBPost_updateProduct13_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/dynamodb/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBPost_updateProduct13_invoker = createInvoker(
    DynamoDBPost_7.updateProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.DynamoDBPost",
      "updateProduct",
      Nil,
      "POST",
      """""",
      this.prefix + """v1/dynamodb/product/"""
    )
  )

  // @LINE:31
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBDelete_deleteProduct14_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/dynamodb/product/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_DynamoDBDelete_deleteProduct14_invoker = createInvoker(
    DynamoDBDelete_0.deleteProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.DynamoDBDelete",
      "deleteProduct",
      Nil,
      "DELETE",
      """""",
      this.prefix + """v1/dynamodb/product/"""
    )
  )

  // @LINE:34
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/cloudsearch/genericAPI/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI15_invoker = createInvoker(
    CloudSearchGet_12.genericAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CloudSearchGet",
      "genericAPI",
      Nil,
      "GET",
      """ Cloud Search based routes""",
      this.prefix + """v1/cloudsearch/genericAPI/"""
    )
  )

  // @LINE:35
  private[this] lazy val ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1.1/cloudsearch/genericAPI/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI16_invoker = createInvoker(
    CloudSearchGet_2.genericAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1_1.controllers.CloudSearchGet",
      "genericAPI",
      Nil,
      "GET",
      """""",
      this.prefix + """v1.1/cloudsearch/genericAPI/"""
    )
  )

  // @LINE:37
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/cloudsearch/structuredSearch/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI17_invoker = createInvoker(
    CloudSearchGet_12.structuredSearchAPI,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CloudSearchGet",
      "structuredSearchAPI",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/cloudsearch/structuredSearch/"""
    )
  )

  // @LINE:38
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchDelete_deleteProduct18_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/cloudsearch/genericAPI/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchDelete_deleteProduct18_invoker = createInvoker(
    CloudSearchDelete_4.deleteProduct,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CloudSearchDelete",
      "deleteProduct",
      Nil,
      "DELETE",
      """""",
      this.prefix + """v1/cloudsearch/genericAPI/"""
    )
  )

  // @LINE:39
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchPut_insertItem19_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/cloudsearch/put/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CloudSearchPut_insertItem19_invoker = createInvoker(
    CloudSearchPut_9.insertItem,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CloudSearchPut",
      "insertItem",
      Nil,
      "PUT",
      """""",
      this.prefix + """v1/cloudsearch/put/"""
    )
  )

  // @LINE:42
  private[this] lazy val ai_commerce_xpresso_v1_controllers_UtilityGet_getBrand20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/util/brand/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_UtilityGet_getBrand20_invoker = createInvoker(
    UtilityGet_13.getBrand,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.UtilityGet",
      "getBrand",
      Nil,
      "GET",
      """ Utility""",
      this.prefix + """v1/util/brand/"""
    )
  )

  // @LINE:45
  private[this] lazy val ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateBrand21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/redis/updateBrands")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateBrand21_invoker = createInvoker(
    BrandsOnRedis_5.updateBrand,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.BrandsOnRedis",
      "updateBrand",
      Nil,
      "GET",
      """ Brand Update into Redis""",
      this.prefix + """v1/redis/updateBrands"""
    )
  )

  // @LINE:46
  private[this] lazy val ai_commerce_xpresso_v1_controllers_BrandsOnRedis_getRedisBrandList22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/redis/getBrands")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_BrandsOnRedis_getRedisBrandList22_invoker = createInvoker(
    BrandsOnRedis_5.getRedisBrandList,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.BrandsOnRedis",
      "getRedisBrandList",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/redis/getBrands"""
    )
  )

  // @LINE:47
  private[this] lazy val ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateDeltaUpdateForBrand23_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/redis/updateDeltaBrands")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateDeltaUpdateForBrand23_invoker = createInvoker(
    BrandsOnRedis_5.updateDeltaUpdateForBrand,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.BrandsOnRedis",
      "updateDeltaUpdateForBrand",
      Nil,
      "POST",
      """""",
      this.prefix + """v1/redis/updateDeltaBrands"""
    )
  )

  // @LINE:50
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CollectionsGet_l124_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/collections/l1/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CollectionsGet_l124_invoker = createInvoker(
    CollectionsGet_11.l1,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CollectionsGet",
      "l1",
      Nil,
      "GET",
      """ Collections GET""",
      this.prefix + """v1/collections/l1/"""
    )
  )

  // @LINE:51
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CollectionsGet_l225_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/collections/l2/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CollectionsGet_l225_invoker = createInvoker(
    CollectionsGet_11.l2,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CollectionsGet",
      "l2",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/collections/l2/"""
    )
  )

  // @LINE:52
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CollectionsGet_l326_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/collections/l3/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_CollectionsGet_l326_invoker = createInvoker(
    CollectionsGet_11.l3,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.CollectionsGet",
      "l3",
      Nil,
      "GET",
      """""",
      this.prefix + """v1/collections/l3/"""
    )
  )

  // @LINE:55
  private[this] lazy val ai_commerce_xpresso_v1_controllers_PromotionsGet_getTodaysPromotions27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("v1/promotions/todayuser/")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_controllers_PromotionsGet_getTodaysPromotions27_invoker = createInvoker(
    PromotionsGet_18.getTodaysPromotions,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.controllers.PromotionsGet",
      "getTodaysPromotions",
      Nil,
      "GET",
      """ Promotions""",
      this.prefix + """v1/promotions/todayuser/"""
    )
  )

  // @LINE:58
  private[this] lazy val ai_commerce_xpresso_v1_swagger_SwaggerController_getResource28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("docs/swagger.json")))
  )
  private[this] lazy val ai_commerce_xpresso_v1_swagger_SwaggerController_getResource28_invoker = createInvoker(
    SwaggerController_8.getResource,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "ai.commerce.xpresso.v1.swagger.SwaggerController",
      "getResource",
      Nil,
      "GET",
      """ Swagger UI""",
      this.prefix + """docs/swagger.json"""
    )
  )

  // @LINE:59
  private[this] lazy val controllers_Assets_at29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("docs/")))
  )
  private[this] lazy val controllers_Assets_at29_invoker = createInvoker(
    Assets_15.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """docs/"""
    )
  )

  // @LINE:60
  private[this] lazy val controllers_Assets_at30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("docs/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_at30_invoker = createInvoker(
    Assets_15.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """docs/""" + "$" + """file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Application_index0_route(params) =>
      call { 
        controllers_Application_index0_invoker.call(Application_16.index)
      }
  
    // @LINE:9
    case ai_commerce_xpresso_v1_controllers_ElasticSearchGet_genericAPI1_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_ElasticSearchGet_genericAPI1_invoker.call(ElasticSearchGet_1.genericAPI)
      }
  
    // @LINE:10
    case ai_commerce_xpresso_v1_controllers_ElasticSearchGet_structuredSearchAPI2_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_ElasticSearchGet_structuredSearchAPI2_invoker.call(ElasticSearchGet_1.structuredSearchAPI)
      }
  
    // @LINE:15
    case ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI3_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI3_invoker.call(CloudSearchGet_12.genericAPI)
      }
  
    // @LINE:16
    case ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI4_route(params) =>
      call { 
        ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI4_invoker.call(CloudSearchGet_2.genericAPI)
      }
  
    // @LINE:18
    case ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI5_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI5_invoker.call(CloudSearchGet_12.structuredSearchAPI)
      }
  
    // @LINE:20
    case ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct6_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct6_invoker.call(DynamoDBGet_17.getProduct)
      }
  
    // @LINE:21
    case ai_commerce_xpresso_v1_controllers_CatalogPut_insertBatch7_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CatalogPut_insertBatch7_invoker.call(CatalogPut_10.insertBatch)
      }
  
    // @LINE:22
    case ai_commerce_xpresso_v1_controllers_CatalogPost_updateProduct8_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CatalogPost_updateProduct8_invoker.call(CatalogPost_3.updateProduct)
      }
  
    // @LINE:23
    case ai_commerce_xpresso_v1_controllers_CatalogDelete_deleteProduct9_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CatalogDelete_deleteProduct9_invoker.call(CatalogDelete_6.deleteProduct)
      }
  
    // @LINE:26
    case ai_commerce_xpresso_v1_controllers_DynamoDBGet_scanAll10_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_DynamoDBGet_scanAll10_invoker.call(DynamoDBGet_17.scanAll)
      }
  
    // @LINE:28
    case ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct11_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_DynamoDBGet_getProduct11_invoker.call(DynamoDBGet_17.getProduct)
      }
  
    // @LINE:29
    case ai_commerce_xpresso_v1_controllers_DynamoDBPut_insertBatch12_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_DynamoDBPut_insertBatch12_invoker.call(DynamoDBPut_14.insertBatch)
      }
  
    // @LINE:30
    case ai_commerce_xpresso_v1_controllers_DynamoDBPost_updateProduct13_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_DynamoDBPost_updateProduct13_invoker.call(DynamoDBPost_7.updateProduct)
      }
  
    // @LINE:31
    case ai_commerce_xpresso_v1_controllers_DynamoDBDelete_deleteProduct14_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_DynamoDBDelete_deleteProduct14_invoker.call(DynamoDBDelete_0.deleteProduct)
      }
  
    // @LINE:34
    case ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI15_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CloudSearchGet_genericAPI15_invoker.call(CloudSearchGet_12.genericAPI)
      }
  
    // @LINE:35
    case ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI16_route(params) =>
      call { 
        ai_commerce_xpresso_v1_1_controllers_CloudSearchGet_genericAPI16_invoker.call(CloudSearchGet_2.genericAPI)
      }
  
    // @LINE:37
    case ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI17_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CloudSearchGet_structuredSearchAPI17_invoker.call(CloudSearchGet_12.structuredSearchAPI)
      }
  
    // @LINE:38
    case ai_commerce_xpresso_v1_controllers_CloudSearchDelete_deleteProduct18_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CloudSearchDelete_deleteProduct18_invoker.call(CloudSearchDelete_4.deleteProduct)
      }
  
    // @LINE:39
    case ai_commerce_xpresso_v1_controllers_CloudSearchPut_insertItem19_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CloudSearchPut_insertItem19_invoker.call(CloudSearchPut_9.insertItem)
      }
  
    // @LINE:42
    case ai_commerce_xpresso_v1_controllers_UtilityGet_getBrand20_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_UtilityGet_getBrand20_invoker.call(UtilityGet_13.getBrand)
      }
  
    // @LINE:45
    case ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateBrand21_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateBrand21_invoker.call(BrandsOnRedis_5.updateBrand)
      }
  
    // @LINE:46
    case ai_commerce_xpresso_v1_controllers_BrandsOnRedis_getRedisBrandList22_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_BrandsOnRedis_getRedisBrandList22_invoker.call(BrandsOnRedis_5.getRedisBrandList)
      }
  
    // @LINE:47
    case ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateDeltaUpdateForBrand23_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_BrandsOnRedis_updateDeltaUpdateForBrand23_invoker.call(BrandsOnRedis_5.updateDeltaUpdateForBrand)
      }
  
    // @LINE:50
    case ai_commerce_xpresso_v1_controllers_CollectionsGet_l124_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CollectionsGet_l124_invoker.call(CollectionsGet_11.l1)
      }
  
    // @LINE:51
    case ai_commerce_xpresso_v1_controllers_CollectionsGet_l225_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CollectionsGet_l225_invoker.call(CollectionsGet_11.l2)
      }
  
    // @LINE:52
    case ai_commerce_xpresso_v1_controllers_CollectionsGet_l326_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_CollectionsGet_l326_invoker.call(CollectionsGet_11.l3)
      }
  
    // @LINE:55
    case ai_commerce_xpresso_v1_controllers_PromotionsGet_getTodaysPromotions27_route(params) =>
      call { 
        ai_commerce_xpresso_v1_controllers_PromotionsGet_getTodaysPromotions27_invoker.call(PromotionsGet_18.getTodaysPromotions)
      }
  
    // @LINE:58
    case ai_commerce_xpresso_v1_swagger_SwaggerController_getResource28_route(params) =>
      call { 
        ai_commerce_xpresso_v1_swagger_SwaggerController_getResource28_invoker.call(SwaggerController_8.getResource)
      }
  
    // @LINE:59
    case controllers_Assets_at29_route(params) =>
      call(Param[String]("path", Right("/public/swagger-ui")), Param[String]("file", Right("index.html"))) { (path, file) =>
        controllers_Assets_at29_invoker.call(Assets_15.at(path, file))
      }
  
    // @LINE:60
    case controllers_Assets_at30_route(params) =>
      call(Param[String]("path", Right("/public/swagger-ui")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at30_invoker.call(Assets_15.at(path, file))
      }
  }
}
