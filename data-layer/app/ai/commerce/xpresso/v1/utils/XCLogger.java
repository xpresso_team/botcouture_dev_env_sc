package ai.commerce.xpresso.v1.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XCLogger {

	private static Logger spLogger = LoggerFactory.getLogger("server");

	public static Logger getSpLogger() {
		return spLogger;
	}

}
