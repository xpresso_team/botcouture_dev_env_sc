package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearch;
import ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB;
import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by Naveen on 1/5/17.
 *
 * Handles all post request for the AWS Dynamodb.
 * Uses AWS SDK to perform put item request to the database.
 *
 * It supports batch request to the server. By default always batch update is performed.
 *
 * It has following api:
 *  1) put list of products in dynamodb.
 */
public class CatalogPut extends Controller {

    @Inject
    private Configuration mConfiguration;

    private AmazonDynamoDB mAmazonDynamoDB = null;

    private AmazonCloudSearch mCloudSearch = null;

    @Inject
    public CatalogPut(Configuration configuration){

        mCloudSearch = new AmazonCloudSearch(configuration);
        mAmazonDynamoDB = new AmazonDynamoDB(configuration);
    }


    /**
         * Fetch single procuct using the primary key eg. xc_sku
         * @return Returns the complete detail of the products
         */
    public Result insertBatch(){

        JSONObject finalResults = new JSONObject();
        JSONObject productData = null;
        try {
            productData= new JSONObject(request().body().asJson().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Check if productData is null
        if(productData == null){
            return ok(ErrorHandler
                    .generateError(400,"fail","Invalid Json")
                    .toString()).as("application/json");
        }


        String tableName = mConfiguration.getString("datalayer.dynamodb.default_table_name");
        String keyName = mConfiguration.getString("datalayer.dynamodb.default_key_name");

        if( productData.has("tableName")){
            try {
                tableName = productData.getString("tableName");
            } catch (JSONException e) {
                return ok(ErrorHandler
                        .generateError(400,"fail","Invalid Json")
                        .toString()).as("application/json");
            }
        }
        if( productData.has("keyName")){

            try {
                keyName = productData.getString("keyName");
            } catch (JSONException e) {
                return ok(ErrorHandler
                        .generateError(400,"fail","Invalid keyName")
                        .toString()).as("application/json");
            }
        }

        try {
            if( !productData.has("items") ||
                productData.getJSONArray("items").length() == 0){

                return ok(ErrorHandler
                        .generateError(400,"fail","Empty items provided")
                        .toString()).as("application/json");
            }else{
                // Update Dynamodb and cloud search
                finalResults = mAmazonDynamoDB.insertProduct(
                        tableName,
                        keyName,
                        productData.getJSONArray("items"));

                if( tableName.equals(
                        mConfiguration.getString("datalayer.dynamodb.default_table_name"))) {

                    mCloudSearch.insertItems(productData.getJSONArray("items"));
                    finalResults.put("searchengine_put","ok");
                }else{
                    finalResults.put("searchengine_put","failed");
                }
            }
        } catch (JSONException e) {
            return ok(ErrorHandler
                    .generateError(400,"fail","Invalid Json")
                    .toString()).as("application/json");
        }

        return ok(finalResults.toString()).as("application/json");
    }
}
