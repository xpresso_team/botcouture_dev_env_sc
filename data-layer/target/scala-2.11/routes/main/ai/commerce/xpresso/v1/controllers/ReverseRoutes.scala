
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:9
package ai.commerce.xpresso.v1.controllers {

  // @LINE:20
  class ReverseDynamoDBGet(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def getProduct(): Call = {
    
      () match {
      
        // @LINE:20
        case ()  =>
          import ReverseRouteContext.empty
          Call("GET", _prefix + { _defaultPrefix } + "v1/catalog/product/")
      
      }
    
    }
  
    // @LINE:26
    def scanAll(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/dynamodb/scan/")
    }
  
  }

  // @LINE:15
  class ReverseCloudSearchGet(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:18
    def structuredSearchAPI(): Call = {
    
      () match {
      
        // @LINE:18
        case ()  =>
          import ReverseRouteContext.empty
          Call("GET", _prefix + { _defaultPrefix } + "v1/catalog/structuredSearch/")
      
      }
    
    }
  
    // @LINE:15
    def genericAPI(): Call = {
    
      () match {
      
        // @LINE:15
        case ()  =>
          import ReverseRouteContext.empty
          Call("GET", _prefix + { _defaultPrefix } + "v1/catalog/genericAPI/")
      
      }
    
    }
  
  }

  // @LINE:21
  class ReverseCatalogPut(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:21
    def insertBatch(): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "v1/catalog/product/")
    }
  
  }

  // @LINE:30
  class ReverseDynamoDBPost(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:30
    def updateProduct(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "v1/dynamodb/product/")
    }
  
  }

  // @LINE:23
  class ReverseCatalogDelete(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:23
    def deleteProduct(): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "v1/catalog/product/")
    }
  
  }

  // @LINE:42
  class ReverseUtilityGet(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:42
    def getBrand(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/util/brand/")
    }
  
  }

  // @LINE:55
  class ReversePromotionsGet(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:55
    def getTodaysPromotions(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/promotions/todayuser/")
    }
  
  }

  // @LINE:39
  class ReverseCloudSearchPut(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def insertItem(): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "v1/cloudsearch/put/")
    }
  
  }

  // @LINE:45
  class ReverseBrandsOnRedis(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:46
    def getRedisBrandList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/redis/getBrands")
    }
  
    // @LINE:45
    def updateBrand(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/redis/updateBrands")
    }
  
    // @LINE:47
    def updateDeltaUpdateForBrand(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "v1/redis/updateDeltaBrands")
    }
  
  }

  // @LINE:29
  class ReverseDynamoDBPut(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def insertBatch(): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "v1/dynamodb/product/")
    }
  
  }

  // @LINE:22
  class ReverseCatalogPost(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def updateProduct(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "v1/catalog/product/")
    }
  
  }

  // @LINE:38
  class ReverseCloudSearchDelete(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:38
    def deleteProduct(): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "v1/cloudsearch/genericAPI/")
    }
  
  }

  // @LINE:9
  class ReverseElasticSearchGet(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:10
    def structuredSearchAPI(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/elasticsearch/structuredSearch/")
    }
  
    // @LINE:9
    def genericAPI(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/elasticsearch/genericAPI/")
    }
  
  }

  // @LINE:31
  class ReverseDynamoDBDelete(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:31
    def deleteProduct(): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "v1/dynamodb/product/")
    }
  
  }

  // @LINE:50
  class ReverseCollectionsGet(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:50
    def l1(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/collections/l1/")
    }
  
    // @LINE:51
    def l2(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/collections/l2/")
    }
  
    // @LINE:52
    def l3(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "v1/collections/l3/")
    }
  
  }


}
